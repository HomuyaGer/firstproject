#import der libraries und genutze Variablen
import os
import socket
import shutil
from datetime import date
import psutil
import alarm
speicher = shutil.disk_usage(os.getcwd())
speicherverbrauch = shutil.disk_usage(".").used
today = date.today()
hostname = socket.gethostname()
ip_address = socket.gethostbyname(hostname)
hdd = shutil.disk_usage("/")
obj_Disk = psutil.disk_usage('/')
percentage = obj_Disk.percent
processes = psutil.pids()
process_numbers = len(processes)
HELPMENU = False
cpu_usage = psutil.cpu_percent(5)
THRESHOLD_SOFTCAP = 20
THRESHOLD_HARDCAP = 25
PROCESS_THRESHOLD_SOFTCAP = 1
PROCESS_THRESHOLD_HARDCAP = 2
CPU_THRESHOLD_SOFTCAP = 1
CPU_THRESHOLD_HARDCAP = 2
obj = alarm.alarm()

#Help Menü mit Ausgabe der Schwellenwerte
while HELPMENU is False:
    userinput = input("Mit \"h\" sehen sie alle Befehle\n")
    if userinput == "quit":
        HELPMENU = True
    elif userinput == "h":
        print ("Für eine Prozessüberprüfung geben sie \"Prozesse\" ein.")
        print ("Für eine Festplattenüberprüfung geben sie \"Festplatte\" ein")
        print ("Für eine Prozessorüberprüfung geben sie \"CPU\" ein")
    if userinput == "Festplatte":
        obj.logini(hostname, ip_address)
        obj.warningHDD(THRESHOLD_SOFTCAP, percentage)
        obj.bigwarningHDD(THRESHOLD_HARDCAP, percentage)
    if userinput == "Prozesse":
        obj.logini(hostname, ip_address)
        obj.warningprocesses(process_numbers, PROCESS_THRESHOLD_SOFTCAP)
        obj.warningprocessbig(process_numbers,PROCESS_THRESHOLD_HARDCAP)
    if userinput == "CPU":
        obj.logini(hostname, ip_address)
        obj.cpuwarning(cpu_usage, CPU_THRESHOLD_SOFTCAP)
        obj.cpuwarningbig(cpu_usage, CPU_THRESHOLD_HARDCAP)
