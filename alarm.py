#Logging-Modul wird importiert
import logging
class alarm:
    logger = None
#Logger wird definiert so wie alle benötigten Funktionen
    def __init__(self):
        logging.basicConfig(filename="info.log", 
					format='%(asctime)s %(message)s', 
                    encoding = "UTF-8",
					filemode='w') 
        self.logger=logging.getLogger() 
        self.logger.setLevel(logging.DEBUG)

    def logini (self, hostname, ip_address):
        self.logger.info (hostname + " IP:" + ip_address)

    def warningHDD (self, THRESHOLD_SOFTCAP, percentage):
        if THRESHOLD_SOFTCAP < percentage:
             self.logger.info("Warnung: System verwendet über 50% des Speicherplatzes")
             return "true"
        else:
             return "false"

    def bigwarningHDD (self, THRESHOLD_HARDCAP, percentage):
        if THRESHOLD_HARDCAP < percentage:
            self.logger.warning("Warnung: System verwendet über 80% des Speicherplatzes")

    def warningprocesses (self, process_numbers,PROCESS_THRESHOLD_SOFTCAP):
        if process_numbers > PROCESS_THRESHOLD_SOFTCAP:
            self.logger.warning("Warnung: System nutzt 250 Prozesse!")
            return "true"
        else:
            return "false"

    def warningprocessbig (self, process_numbers,PROCESS_THRESHOLD_HARDCAP):
        if process_numbers > PROCESS_THRESHOLD_HARDCAP:
            self.logger.warning("Warnung: System nutzt 350 Prozesse!")

    def cpuwarning (self, cpu_usage, CPU_THRESHOLD_SOFTCAP):
        if cpu_usage > CPU_THRESHOLD_SOFTCAP:
            self.logger.warning("Warning System nutzt mehr wie 25 Prozent der CPU Leistung")
            return "true"
        else:
            return "false"

    def cpuwarningbig (self, cpu_usage, CPU_THRESHOLD_HARDCAP):
        if cpu_usage > CPU_THRESHOLD_HARDCAP:
            self.logger.warning("Warning System nutzt mehr wie 50 Prozent der CPU Leistung")