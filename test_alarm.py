import unittest
from alarm import *

class alarmtestcase(unittest.TestCase):

    def setUp(self):
        self.obj = alarm()

    def test_passes_alarmHDD(self):
        self.assertEqual("true", self.obj.warningHDD(20, 50))
        self.assertEqual("false", self.obj.warningHDD(50, 30))

    def test_passes_alarmProcess(self):
        self.assertEqual("true", self.obj.warningprocesses(100, 20))
        self.assertEqual("false", self.obj.warningprocesses(80, 100))
    
    def test_passes_alarmCPU(self):
        self.assertEqual("true", self.obj.cpuwarning(100,50))
        self.assertEqual("false", self.obj.cpuwarning(50,75))


if __name__=='__main__':
    unittest.main()